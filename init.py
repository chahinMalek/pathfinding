import ctypes
import json

import pygame

from algorithms import a_star
from tsp import closest_neighbour_tour, tour_cost, a_star_tsp

with open('./resources/djibouti.json') as f:
    cities = json.load(f)

with open('./resources/djibouti_original.json') as f:
    distances = json.load(f)

with open('./resources/djibouti_adj_matrix.json') as g:
    adj_matrix = json.load(g)

# configuration
pygame.init()
ctypes.windll.user32.SetProcessDPIAware()
clock = pygame.time.Clock()
info = pygame.display.Info()

# colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)

width, height = info.current_w, info.current_h

screen = pygame.display.set_mode(
    (width, height),
    pygame.FULLSCREEN
)

pygame.display.set_caption('coins')
pygame.display.flip()
font = pygame.font.SysFont('Arial', 30)

# functions
draw_circle = pygame.draw.circle
draw_line = pygame.draw.line

# variables
path = a_star('1', '38', cities, adj_matrix)
# path = closest_neighbour_tour(cities, "5", set(), adj_matrix)
# path = a_star_tsp('14', cities, adj_matrix)
# print(tour_cost(path, distances))
running = True

while running:

    screen.fill(WHITE)

    for city in cities:
        point = cities[city]
        h, w = int(point[0]), int(point[1])
        draw_circle(screen, BLACK, (h, w), 3)
        label = font.render(city, 1, BLUE)
        screen.blit(label, (h-2, w-2))

    for row in adj_matrix:
        for node in adj_matrix[row]:
            draw_line(screen, BLACK, cities[node], cities[row], 3)

    for i in range(len(path)-1):
        draw_line(screen, RED, cities[path[i]], cities[path[i+1]], 3)

    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            running = not running
            break

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = not running
                break

    pygame.display.update()
    clock.tick(60)

pygame.quit()

