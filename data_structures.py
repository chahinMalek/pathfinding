from typing import Generic, TypeVar, List, Dict

_T = TypeVar('_T')


class Heap(Generic[_T]):

    def __init__(self, iterable: List[_T]):

        self.heap = list(iterable)

        if len(self.heap) != 0:
            self.__make_heap()

    @staticmethod
    def __get_parent(index: int) -> int:
        return index // 2

    @staticmethod
    def __get_left_child(index: int) -> int:
        return 2 * index + 1

    @staticmethod
    def __get_right_child(index: int) -> int:
        return 2 * index + 2

    def __up_heap(self, index: int) -> int:

        if not 0 <= index < len(self.heap):
            raise IndexError('Index out of bounds exception!')

        if index == 0:
            return index

        if 0 < index < len(self.heap):
            p_index: int = Heap.__get_parent(index)

            if self.heap[index][0] < self.heap[p_index][0]:
                self.heap[p_index], self.heap[index] = self.heap[index], self.heap[p_index]

            return self.__up_heap(p_index)

    def __down_heap(self, index: int) -> int:

        if not 0 <= index < len(self.heap):
            raise IndexError('Index out of bounds exception!')

        l_child: int = Heap.__get_left_child(index)

        if len(self.heap) <= l_child:
            return index

        r_child: int = l_child + 1 if l_child + 1 < len(self.heap) else l_child

        if self.heap[r_child][0] < self.heap[l_child][0]:
            l_child = r_child

        if self.heap[l_child][0] < self.heap[index][0]:

            self.heap[l_child], self.heap[index] = self.heap[index], self.heap[l_child]
            return self.__down_heap(l_child)

        else:
            return index

    def __make_heap(self) -> None:
        index: int = len(self.heap) // 2

        while index >= 0:
            self.__down_heap(index)
            index -= 1

    def is_empty(self) -> bool:
        return len(self.heap) == 0

    def add(self, item: _T, priority: float) -> None:
        self.heap.append((priority, item))
        self.__up_heap(len(self.heap)-1)

    def top(self) -> _T:
        return self.heap[0] if len(self.heap) > 0 else None

    def get(self) -> _T:

        if len(self.heap) == 0:
            return None

        self.heap[0], self.heap[-1] = self.heap[-1], self.heap[0]
        item: _T = self.heap.pop()

        if len(self.heap) > 0:
            self.__down_heap(0)

        return item


class Graph(Generic[_T]):

    def __init__(self):
        self.nodes = dict()

    def add_node(self, label: str):

        if label not in self.nodes:
            self.nodes[label] = dict()

    def remove_node(self, label: str):

        for node in self.nodes[label].keys():
            self.nodes[node].pop(label)

        self.nodes.pop(label)

    def add_edge(self, first: str, other: str, weight):

        self.nodes[first][other] = weight
        self.nodes[other][first] = weight

    def remove_edge(self, first: str, other: str):
        self.nodes[first].pop(other)
        self.nodes[other].pop(first)

    def get_neighbours(self, label: str) -> Dict:
        return self.nodes[label]
