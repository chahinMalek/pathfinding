from math import sqrt
from typing import Dict

from data_structures import Heap


def euclidean(node1, node2):
    return sqrt((node1[0] - node2[0]) ** 2 + (node1[1] - node2[1]) ** 2)


def a_star(start, goal, nodes: Dict, adj_matrix: Dict):

    if start not in nodes or goal not in nodes:
        raise ValueError('Method parameters must be contained in the node list.')

    parent = {start: None}
    g = {start: 0}
    h = {node: euclidean(nodes[node], nodes[goal]) for node in nodes}

    frontier = Heap([])
    frontier.add(start, 0)

    while not frontier.is_empty():

        node = frontier.get()[1]

        if node == goal:
            break

        neighbours = adj_matrix[node]

        for neighbour in neighbours:
            cost = g[node] + euclidean(nodes[node], nodes[neighbour])

            if neighbour not in g or cost < g[neighbour]:
                g[neighbour] = cost
                frontier.add(neighbour, cost + h[neighbour])
                parent[neighbour] = node

    path = [goal]
    node = goal

    while node != start:
        node = parent[node]
        path.append(node)

    path.reverse()
    return path
