import json

file_name = 'djibouti.json'

with open(file_name) as f:
    cities = json.load(f)


kvocijent1 = 1.5
kvocijent2 = 2
z = 7150
y = 20900

for city in cities:
    cities[city][0] = cities[city][0] / kvocijent1 - z
    cities[city][1] = cities[city][1] / kvocijent2 - y

with open(file_name, 'w') as out:
    json.dump(cities, out, separators=(', ', ':'), indent=4)
