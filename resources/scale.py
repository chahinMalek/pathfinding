import json

file_name = 'djibouti.json'

with open(file_name) as f:
    cities = json.load(f)

for city in cities:
    cities[city][0] *= 1.15
    cities[city][1] *= 1.15

with open(file_name, 'w') as out:
    json.dump(cities, out, separators=(', ', ':'), indent=4)
