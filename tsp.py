import sys
from typing import List, Set, Dict

from algorithms import euclidean
from data_structures import Heap


def is_valid_tour(tour: List, size: int) -> bool:

    """
        We say that a tour is valid if:
            1. It is of size n
            2. Does not contain cycles
    """

    if len(tour) != size:
        return False

    visited = set()

    for node in tour:

        # handles cycles
        if node in visited:
            return False

        visited.add(node)

    return True


def tour_cost(path: List, nodes: Dict) -> float:

    """
        Returns the total tour cost calculated by summing up weights on path edges
    """

    return path_cost(path, nodes) + euclidean(nodes[path[-1]], nodes[path[0]])


def path_cost(path: List, nodes: Dict) -> float:

    """
        Returns the total path cost calculated by summing up weights on path edges
    """

    cost = 0.0

    for i in range(len(path) - 1):
        cost += euclidean(nodes[path[i]], nodes[path[i + 1]])

    return cost


def closest_neighbour_tour(nodes: Dict, start, visited: Set, adj_matrix) -> List:

    if start in visited:
        raise ValueError("Starting node already visited")

    v_size = len(visited)
    tour = [start]
    visited.add(start)
    last = tour[-1]
    keys = list(nodes.keys())

    for i in range(len(keys)-v_size-1):

        min_distance = sys.maxsize
        closest = None
        neighbours = adj_matrix[last]

        for j in range(len(neighbours)):

            if neighbours[j] in visited:
                continue

            distance = euclidean(nodes[last], nodes[neighbours[j]])
            if distance < min_distance:
                min_distance = distance
                closest = neighbours[j]

        if closest is None:
            raise ValueError('Tour could not be formed')

        visited.add(closest)
        tour.append(closest)
        last = closest

    if is_valid_tour(tour, len(nodes)-v_size):
        return tour
    else:
        raise RuntimeError("A valid tour could not be formed!")


def a_star_tsp(start, nodes: Dict, adj_matrix: Dict):

    def get_tour(start, node, parent_dict):

        path = [node]
        current = node

        while current != start:
            current = parent_dict[current]
            path.append(current)

        path.reverse()
        return path

    def h(node, visited):

        v = set(visited)
        cost = path_cost(closest_neighbour_tour(nodes, node, v, adj_matrix), nodes)
        return cost

    if start not in nodes:
        raise ValueError('Method parameter must be contained in the node list.')

    size = len(nodes)
    parent = {start: None}
    g = {start: 0}

    frontier = Heap([])
    frontier.add(start, 0)

    while not frontier.is_empty():

        node = frontier.get()[1]
        path = get_tour(start, node, parent)
        print(path)

        if is_valid_tour(path, size):
            break

        neighbours = [n for n in adj_matrix[node] if n not in path]

        for neighbour in neighbours:
            cost = g[node] + euclidean(nodes[node], nodes[neighbour])

            if neighbour not in g or cost < g[neighbour]:
                g[neighbour] = cost
                frontier.add(neighbour, (cost + h(neighbour, path)))
                parent[neighbour] = node

    return path
